package game

import "time"

const (
	eroToEarSM = 12.0 / 35.0
	eroToEarMM = 1.0 / 175.0
	eroToEarDM = 1.0 / 4200.0
)

func ErozaTime(date time.Time) (day, bel, min uint) {
	sc := float64(date.UnixMilli()) / 1000.0
	min = uint(sc*eroToEarSM) % 60
	bel = uint(sc*eroToEarMM) % 24
	day = uint(sc*eroToEarDM) % 32
	return
}
