package game

import (
	"time"

	"gitlab.com/cwink/cwxiv/data"
)

func calcWeatherIdx(date time.Time) uint32 {
	sc := float64(date.UnixMilli()) / 1000.0
	bl := uint32(sc * eroToEarMM)
	dy := uint32(sc * eroToEarDM)
	inc := (bl + 8 - (bl % 8)) % 24
	bs := dy*100 + inc
	s1 := (bs << 11) ^ bs
	return ((s1 >> 8) ^ s1) % 100
}

func CurrentWeather(place string) (weather string, err error) {
	idx := calcWeatherIdx(time.Now())
	weather, err = data.GetWeather(place, idx)
	return
}
