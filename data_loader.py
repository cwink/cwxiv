"""
This module is used to download FFXIV CSV data and load it into a normalized relational database.
The SQL code is printed to STDOUT and meant to be piped to a SQL file to be ran in a database
manager or piped to a CLI utility. I.e. to create a SQLite DB:

    python3 data_loader.py | sqlite3 cwxiv.db

Or to place it into a script to run in some GUI:

    python3 data_loader.py > cwxiv.sql
"""

import codecs
import csv
import urllib.request

PLACE_NAME_URL = 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master' \
    '/csv/PlaceName.csv'
PLACE_NAME_TABLE_NAME = 'place_name'

print(f"""
    CREATE TABLE IF NOT EXISTS {PLACE_NAME_TABLE_NAME} (
        id          INTEGER NOT NULL PRIMARY KEY,
        name        TEXT
    );
 """)

with urllib.request.urlopen(PLACE_NAME_URL) as page:
    if page.status != 200:
        raise Exception(f"invalid status code {page.status} returned")
    reader = csv.reader(codecs.iterdecode(page, 'utf-8'))
    next(reader)
    next(reader)
    next(reader)
    for row in reader:
        print(f"""
        INSERT INTO {PLACE_NAME_TABLE_NAME} (id, name)
        VALUES ({row[0]}, "{row[1]}");
    """)

TERRITORY_TYPE_URL = 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master' \
    '/csv/TerritoryType.csv'
TERRITORY_TYPE_TABLE_NAME = 'territory_type'

print(f"""
    CREATE TABLE IF NOT EXISTS {TERRITORY_TYPE_TABLE_NAME} (
        id           INTEGER NOT NULL PRIMARY KEY,
        region       INTEGER,
        zone         INTEGER,
        place        INTEGER,
        weather_rate INTEGER,
        FOREIGN KEY(region)       REFERENCES place_name(id),
        FOREIGN KEY(zone)         REFERENCES place_name(id),
        FOREIGN KEY(place)        REFERENCES place_name(id),
        FOREIGN KEY(weather_rate) REFERENCES weather_rate(id)
    );
""")

with urllib.request.urlopen(TERRITORY_TYPE_URL) as page:
    if page.status != 200:
        raise Exception(f"invalid status code {page.status} returned")
    reader = csv.reader(codecs.iterdecode(page, 'utf-8'))
    next(reader)
    next(reader)
    next(reader)
    for row in reader:
        print(f"""
            INSERT INTO {TERRITORY_TYPE_TABLE_NAME} (id, region, zone, place, weather_rate)
            VALUES ({row[0]}, {row[4]}, {row[5]}, {row[6]}, {row[13]});
        """)

WEATHER_URL = 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master/csv/Weather.csv'
WEATHER_TABLE_NAME = 'weather'

print(f"""
   CREATE TABLE IF NOT EXISTS {WEATHER_TABLE_NAME} (
       id          INTEGER NOT NULL PRIMARY KEY,
       name        TEXT,
       description TEXT
   );
""")

with urllib.request.urlopen(WEATHER_URL) as page:
    if page.status != 200:
        raise Exception(f"invalid status code {page.status} returned")
    reader = csv.reader(codecs.iterdecode(page, 'utf-8'))
    next(reader)
    next(reader)
    next(reader)
    for row in reader:
        print(f"""
           INSERT INTO {WEATHER_TABLE_NAME} (id, name, description)
           VALUES ({row[0]}, "{row[2]}", "{row[3]}");
       """)

WEATHER_RATE_URL = 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master' \
    '/csv/WeatherRate.csv'
WEATHER_RATE_TABLE_NAME = 'weather_rate'

print(f"""
   CREATE TABLE IF NOT EXISTS {WEATHER_RATE_TABLE_NAME} (
       id INTEGER NOT NULL PRIMARY KEY
   );

   CREATE TABLE IF NOT EXISTS {WEATHER_RATE_TABLE_NAME}s (
       id           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       weather_rate INTEGER,
       weather      INTEGER,
       rate         INTEGER,
       FOREIGN KEY(weather_rate) REFERENCES weather_rate(id),
       FOREIGN KEY(weather)      REFERENCES weather(id)
   );
""")

with urllib.request.urlopen(WEATHER_RATE_URL) as page:
    if page.status != 200:
        raise Exception(f"invalid status code {page.status} returned")
    reader = csv.reader(codecs.iterdecode(page, 'utf-8'))
    next(reader)
    next(reader)
    next(reader)
    for row in reader:
        print(f"""
           INSERT INTO {WEATHER_RATE_TABLE_NAME} (id)
           VALUES ({row[0]});
       """)
        for i in range(1, 17, 2):
            print(f"""
               INSERT INTO {WEATHER_RATE_TABLE_NAME}s (weather_rate, weather, rate)
               VALUES ({row[0]}, {row[i]}, {row[i + 1]});
           """)
