package data

func GetWeather(place string, idx uint32) (weather string, err error) {
	query := `
		SELECT weather
		FROM
			(
				SELECT
					w.name                                                     AS weather,
					(-1 * (wrs.rate - (SUM(wrs.rate) OVER (ORDER BY wrs.id)))) AS rate_low,
					(SUM(wrs.rate) OVER (ORDER BY wrs.id))                     AS rate_high
				FROM weather_rate wr
				JOIN
					territory_type tt  ON wr.id       = tt.weather_rate,
					weather_rates  wrs ON wr.id       = wrs.weather_rate,
					weather        w   ON wrs.weather = w.id,
					place_name     pn  ON tt.place    = pn.id
				WHERE
					pn.name  = ?
				ORDER BY wrs.id
			)
		WHERE
			? >= rate_low  AND 
			? <  rate_high
		;
	`
	row := dbCtx.QueryRow(query, place, idx, idx)
	err = row.Scan(&weather)
	return
}
