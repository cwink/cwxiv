package data

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

var dbCtx *sql.DB

func ConnectToDB() error {
	var err error
	if _, err = os.Stat("cwxiv.db"); err != nil {
		return fmt.Errorf("unable to open database file: %w", err)
	}
	if dbCtx, err = sql.Open("sqlite3", "file:cwxiv.db?cache=shared&mode=ro&immutable=true"); err != nil {
		return fmt.Errorf("failed to connect to database: %w", err)
	}
	return nil
}

func DisconnFromDB() error {
	if err := dbCtx.Close(); err != nil {
		return fmt.Errorf("failed to disconnect from database: %w", err)
	}
	return nil
}
