package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/cwink/cwxiv/data"
	"gitlab.com/cwink/cwxiv/game"
)

func main() {
	if err := data.ConnectToDB(); err != nil {
		log.Fatal(err)
	}
	day, bel, min := game.ErozaTime(time.Now())
	fmt.Printf("%2.2v %2.2v:%2.2v\n", day, bel, min)
	weather, err := game.CurrentWeather("Unnamed Island")
	if err != nil {
		log.Print(err)
	}
	fmt.Println(weather)
	if err := data.DisconnFromDB(); err != nil {
		log.Print(err)
	}
}
